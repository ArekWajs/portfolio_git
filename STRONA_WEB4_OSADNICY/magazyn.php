<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>




<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />

 <script>
        var $timerID = null,
            $dif = (new Date(<?php echo militime();?>)).getTime() - (new Date()).getTime();

        function wyswietlCzas() {
            var $data = new Date((new Date()).getTime() + $dif),
                $godziny = $data.getHours(),
                $minuty = $data.getMinutes(),
                $sekundy = $data.getSeconds(),
                $czas = ['<b>', $godziny, ':', ($minuty < 10) ? '0'.concat($minuty) : $minuty, ':', ($sekundy < 10) ? '0'.concat($sekundy) : $sekundy, '</b>'].join('');
            document.getElementById("zegarLayer").innerHTML = $czas;
            $timerID = setTimeout(wyswietlCzas, 1000);
        }
        window.onload = wyswietlCzas;

    </script>

</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu active" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>

                <div class="surowce-area">

                    <div class="wyloguj-surowce">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>


                    <div class="zasoby-surowce">
                        <?php
	echo "<p><b>Drewno</b>: ".$_SESSION['drewno'];
    echo " | <b>Glina</b>: ".$_SESSION['glina'];
    echo " | <b>Kamień</b>: ".$_SESSION['kamien'];
    echo " | <b>Zboże</b>: ".$_SESSION['zboze']."</p>";
                                               
	           ?>


                    </div>

                    <div class="surowce">
                        <h3>Magazyn surowców</h3>
                        </br>
                        <p>Wymagania: Główny budynek poziom 1</p>
                        </br>
                        <p>W Magazynie surowców przechowywane jest drewno, glina oraz żelazo. Poprzez rozbudowę budynku zwiększa się pojemność Magazynu. Po rozbudowaniu Magazynu surowców na 20 poziom możesz budować kolejne Magazyny.</p>
                        
                        <img src="icon/magazyn1.png">
                
                         <?php 

                        echo "<a href='magazyn.php?buduj=magazyn'>Buduj Magazyn</a><br>";
                        echo "<a href='magazyn.php'>Odswiez</a><br><br>";
                        ?>


                    </div>
                </div>
            </article>

        </section>



        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
