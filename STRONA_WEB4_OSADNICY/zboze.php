<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>

<?php
require_once "connect.php";
?>

<?php

    $id = $_SESSION['id'];
    $polaczenie = new mysqli($host, $db_user, $db_password);  
    $db = mysqli_select_db($polaczenie, $db_name);
   
   if(isset($_POST['update']))
    {
        $query = "UPDATE `uzytkownicy` SET zboze='$_POST[zboze]' WHERE `uzytkownicy`.`id` = '$id' ";
        $query_run = mysqli_query($polaczenie,$query);
        
        //if ($polaczenie->query("UPDATE uzytkownicy SET `kamien` = '156111' WHERE `uzytkownicy`.`id` = '$id' "))
        $_SESSION['zboze'] = $_POST['zboze'];
       

    }




$polaczenie -> close();

?>






<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu active" href="gra.php">Okolice</a></li>
                    <li><a class="menu" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>

                <div class="surowce-area">

                    <div class="wyloguj-surowce">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>


                    <div class="zasoby-surowce">
                        <?php
	echo "<p><b>Drewno</b>: ".$_SESSION['drewno'];
    echo " | <b>Glina</b>: ".$_SESSION['glina'];
    echo " | <b>Żelazo</b>: ".$_SESSION['kamien'];
    echo " | <b>Zboże</b>: ".$_SESSION['zboze']."</p>";
                                    
	           ?>
    
                    </div>

                    <div class="surowce">
                        <h3>Pole zboża</h3>
                        </br>
                        <p>Wymagania: brak</p>
                        </br>
                        <p>Tutaj twoi chłopi sieją zboże, które służy do wyżywienia ludności Twojej osady. Podnosząc poziom tego pola zwiększasz przychód zboża.</p>
                        </br>
                        <p>Ciekawostki:</br>
                            * Ciekawostka: teoretycznie w stolicy możesz rozbudowywać Pole zboża do 25 lv, jednak w rzeczywistości jego maksymalny poziom to 19. Surowce potrzebne na wyższe poziomy nie byłby w stanie zmieścić się w magazynach, nawet gdyby zajmowały one wszystkie pola w osadzie.</p>
                        <img src="icon/zboze2.png">

                        <form action="" method="post">
                            <input type="text" name="zboze" placeholder="Dodaj zboze" />
                            <input type="submit" name="update" value="UPDATE DATA" />

                        </form>
                    </div>
                </div>
            </article>

        </section>



        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
