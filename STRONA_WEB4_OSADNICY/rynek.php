<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>




<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu active" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>

                <div class="surowce-area">

                    <div class="wyloguj-surowce">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>


                    <div class="zasoby-surowce">
                        <?php
	echo "<p><b>Drewno</b>: ".$_SESSION['drewno'];
    echo " | <b>Glina</b>: ".$_SESSION['glina'];
    echo " | <b>Kamień</b>: ".$_SESSION['kamien'];
    echo " | <b>Zboże</b>: ".$_SESSION['zboze']."</p>";
                                               
	           ?>


                    </div>

                    <div class="surowce">
                        <h3>Rynek</h3>
                        </br>
                        <p>Wymagania: Główny budynek poziom 3, Magazyn poziom 1</p>
                        </br>
                        <p>Poprzez Rynek możesz rozsyłać surowce i handlować nimi z innymi graczami. Przy każdym poziomie budynku pojawia się kolejny handlarz do dyspozycji, który będzie mógł transportować określoną ilość surowców. Rozbudowa Rynku na 20 poziom, daje możliwość zbudowania Targu, który to może potroić udźwig handlarzy w wiosce.</p>
                        </br>
                        <p>Ciekawostki:</br>
                            * Wbrew temu co niektórzy myślą i mówią, zburzenie Rynku jest równoznaczne z utratą handlarzy/możliwości transportu surowców z danej osady, czyli np. jeśli podczas ataku lub też samemu poprzez opcje burzenie budynków, obniży się poziom Rynku z 20 na 15, to traci się handlarzy z 20 do 15 sztuk.</br>
                            * Rynek jest potrzebny do wysyłania surowców z danej osady, jeśli nie posiadasz rynku w jakiejś wiosce, oznacza to że nie wyślesz z niej żadnych surowców, natomiast do niej transporty mogą docierać.</br>
                            * Podczas ataku na Twoją osadę, surowce wystawione na rynku także są grabione. Jedynie surowce które już są w drodze do innych wiosek pozostają bezpieczne.</p>
                        <img src="icon/rynek.png">

                    </div>
                </div>
            </article>

        </section>



        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
