<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
        
       
	}
    
    
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>
<?php
require_once "connect.php";

?>

<?php

    $id = $_SESSION['id'];
    $polaczenie = new mysqli($host, $db_user, $db_password);  
    $db = mysqli_select_db($polaczenie, $db_name);
 
    if(isset($_POST['update']))
    {
       
        $query = "UPDATE `uzytkownicy` SET drewno='$_POST[drewno]' WHERE `uzytkownicy`.`id` = '$id' ";
        $query_run = mysqli_query($polaczenie,$query);
        if ($polaczenie->query("UPDATE uzytkownicy SET `glina` = '322' WHERE  `uzytkownicy`.`id` = '$id' "))
        $_SESSION['drewno'] = $_POST['drewno'];
       if($query_run)
        {
            echo'<script type="text/javascript"> alert("Data Updated")</script>';
        }
            else
            {
                echo'<script type="text/javascript">alert("Data Not Updated")</script>';
            }
        
    }
$polaczenie -> close();
?>




<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu active" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>

                <div class="surowce-area">

                    <div class="wyloguj-surowce">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>


                    <div class="zasoby-surowce">
                        <?php
	echo "<p><b>Drewno</b>: ".$_SESSION['drewno'];
    echo " | <b>Glina</b>: ".$_SESSION['glina'];
    echo " | <b>Żelazo</b>: ".$_SESSION['kamien'];
    echo " | <b>Zboże</b>: ".$_SESSION['zboze']."</p>";
                                               
	           ?>


                    </div>

                    <div class="surowce">
                        <h3>Koszary</h3>
                        </br>
                        <p>Wymagania: Główny budynek poziom 3</p>
                        </br>
                        <p>W koszarach trenuje się jednostki piechoty. Im wyższy poziom budynku, tym szybciej będą się szkoliły jednostki.</p>
                        </br>
                        <p>Ciekawostki:</br>
                            * Czas szkolenia jednostek jest zależny od poziomu Koszar w momencie załączenia szkolenia. Czyli np. jeśli załączymy budowę 500 Toporników na 10 poziomie Koszar, a następnie po wyszkoleniu połowy ulepszymy Koszary na 20 poziom, to mimo wyższego poziomu budynku, reszta Toporników będzie się szkoliła w takim samym czasie, dopiero jednostki które się załączy do szkolenia już po ulepszeniu Koszar, będą się szybciej szkoliły.</p>
                        <img src="icon/koszary.png" style="display: block;float: left;">
                        </br>
                        <p><b>Koszt budowy</b></p></br>
                        Drewno: 210 Glina: 140 Żelazo: 260 Zboże: 120
                        </br>
                        <form action="" method="post">
                            <input type="text" name="drewno" placeholder="Enter drewno" />
                            <input type="submit" name="update" value="UPDATE DATA" />

                        </form>

                    </div>








            </article>

        </section>



        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
