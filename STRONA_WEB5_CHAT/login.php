<!--
//login.php
!-->

<?php

include('database_connection.php');

session_start();

$message = '';

if(isset($_SESSION['user_id']))
{
	header('location:index.php');
}

if(isset($_POST['login']))
{
	$query = "
		SELECT * FROM login 
  		WHERE username = :username
	";
	$statement = $connect->prepare($query);
	$statement->execute(
		array(
			':username' => $_POST["username"]
		)
	);	
	$count = $statement->rowCount();
	if($count > 0)
	{
		$result = $statement->fetchAll();
		foreach($result as $row)
		{
			if(password_verify($_POST["password"], $row["password"]))
			{
				$_SESSION['user_id'] = $row['user_id'];
				$_SESSION['username'] = $row['username'];
				$sub_query = "
				INSERT INTO login_details 
	     		(user_id) 
	     		VALUES ('".$row['user_id']."')
				";
				$statement = $connect->prepare($sub_query);
				$statement->execute();
				$_SESSION['login_details_id'] = $connect->lastInsertId();
				header('location:index.php');
			}
			else
			{
				$message = '<label>Wrong Password</label>';
			}
		}
	}
	else
	{
		$message = '<label>Wrong Username</labe>';
	}
}


?>

<html>

<head>
    <title>C H A T</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="icon" href="chat1.ico">
    <link rel="shortcut icon" href="chat1.ico" type="image/x-icon">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>

<body>
    <header>

        <p>C H A T</p>

        <div class="container">

    </header>
    <section>
        <div class="panel panel-default">
            <div class="panel-heading">Chat login</div>
            <div class="panel-body">
                <p class="text-danger"><?php echo $message; ?></p>
                <form method="post">
                    <div class="form-group">
                        <label>Enter Username</label>
                        <input type="text" name="username" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label>Enter Password</label>
                        <input type="password" name="password" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <input type="submit" name="login" class="btn btn-info" value="Login" />
                    </div>
                    <div align="center">
                        <a href="register.php">Register</a>
                    </div>
                </form>

            </div>
        </div>
        </div>
    </section>
    <footer>Arkadiusz Wajs | Chat | 2020
    </footer>
</body>

</html>
<style>
    body {
        padding-top: 18px;
        padding-bottom: 20px;
        font-family: 'Lato', sans-serif;
        font-size: 20px;
        margin: 0 !important;
    }

    header {
        margin-left: 200px;
        margin-right: 200px;
        margin-bottom: auto;
        margin-top: auto;
        text-align: center;
        background-color: #AAA;
        color: white;
        padding: 20px;
        font-size: 30px;


    }

    section {
        margin-left: 200px;
        margin-right: 200px;
        margin-bottom: auto;
        margin-top: auto;
        padding-top: 40px;
        padding-bottom: 50px;

    }

    footer {
        margin-left: 200px;
        margin-right: 200px;
        margin-bottom: auto;
        margin-top: auto;
        text-align: center;
        background-color: #5ca1d2;
        padding: 20px;
        color: white;
        font-size: 18px;

    }

</style>
