<?php

	session_start();
	//session_destroy();
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>


<?php
require_once "connect.php";
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>



<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



    <script>
        var $timerID = null,
            $dif = (new Date(<?php echo militime();?>)).getTime() - (new Date()).getTime();

        function wyswietlCzas() {
            var $data = new Date((new Date()).getTime() + $dif),
                $godziny = $data.getHours(),
                $minuty = $data.getMinutes(),
                $sekundy = $data.getSeconds(),
                $czas = ['<b>', $godziny, ':', ($minuty < 10) ? '0'.concat($minuty) : $minuty, ':', ($sekundy < 10) ? '0'.concat($sekundy) : $sekundy, '</b>'].join('');
            document.getElementById("zegarLayer").innerHTML = $czas;
            $timerID = setTimeout(wyswietlCzas, 1000);
        }
        window.onload = wyswietlCzas;

    </script>




</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu active" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>
                <div class="wiadomosci-area">


                    <div class="wyloguj">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>

                    <div class="dane">
 
                        </br>
                    
                    <div class="zegar" id="zegarLayer"></div>

                    </div>
                    <div class="wiadomosci">
                        
                        <?php
                                              
                        $polaczenie = new mysqli($host, $db_user, $db_password);  
                        $db = mysqli_select_db($polaczenie, $db_name);
 
                        if(isset($_POST['send']))
                        {   
                            $msg = $_POST['msg'];
                            $name = $_SESSION['user'];
                                                    
                            $query = "INSERT INTO `posts` (`msg`, `name`) VALUES ('$msg','$name')";
                           
                            $query_run = mysqli_query($polaczenie,$query);                       
                            $_SESSION['msg'] = $_POST['msg'];
                            if(!empty($_POST['msg'] && $query_run))
                            {
                                
                                echo 'Wiadomość wysłano!';
                                echo "<br>";
                            }
                            else
                            {
                                
                                echo 'Wysłałeś pustą wiadomość!';
                                echo "<br>";
                                
                            }
                             
                        }
                        $polaczenie -> close();
              
                        ?>
                        </br>
                    
  
                        <h3>Skrzynka odbiorcza</h3>
                        <form>
                            <div class="output">
                                <label><textarea name="mailbox" readonly class="wpis1" cols="5" rows="5">
                                <?php
                            $conn = new mysqli($host, $db_user, $db_password, $db_name);
                            // Check connection
                            if ($conn->connect_error) {
                              die("Connection failed: " . $conn->connect_error);
                            }
                            $sql = "SELECT * FROM posts";
                            $result = $conn->query($sql);
                            if($result->num_rows > 0){
                                while ($row = $result->fetch_assoc()) {
                                    echo "".$row["name"].""."-".$row["msg"]."-".$row["date"]."\n";
                                }
                            } else {
                                echo "0 results";
                            }
                            $conn->close();
                            
                            ?>
                                
                                
                                    </textarea></label>
                            </div>
                        </form>
                        
                                              </br>
                        <p>Formularz wysyłania wiadomości</p>
                        <form action="wiadomosci.php" method="post">
                            <div class="send">
                                <textarea name="msg" class="wpis" placeholder="Napisz wiadomość"></textarea>
                                <label><input type="submit" class="button" name="send" value="Wyślij wiadomość" /></label>

                            </div>
                        </form>
                        </br>
                    </div>



                </div>

            </article>
        </section>

        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
