<?php

	session_start();
	
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
	{
		header('Location: gra.php');
		exit();
	}

?>

<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />
</head>

<body>
    <div id="wrapper">
        <header>
            
            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>
            
            <article>

                <h1>Zaloguj się do gry!</h1>
                <form action="zaloguj.php" method="post">
                    <div class="box">
                        <label><span>Login:</span> <br /> <input type="text" class="wpis" name="login" id="login" /></label> <br />
                        <label><span>Hasło:</span> <br /> <input type="password" class="wpis" name="haslo" id="haslo" /> <br /><br /></label>
                        <label><input type="submit" class="button" value="Zaloguj się" /></label>
                    </div>
                </form>
                <br /><br /><br /><br />
                <a href="rejestracja.php">Rejestracja - załóż darmowe konto!</a>
                <br /><br /><br /><br /><br />

                <?php
	if(isset($_SESSION['blad']))	echo $_SESSION['blad'];
?>
            </article>
        </section>
        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>
</body>

</html>
