<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>




<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



    <script>
        var $timerID = null,
            $dif = (new Date(<?php echo militime();?>)).getTime() - (new Date()).getTime();

        function wyswietlCzas() {
            var $data = new Date((new Date()).getTime() + $dif),
                $godziny = $data.getHours(),
                $minuty = $data.getMinutes(),
                $sekundy = $data.getSeconds(),
                $czas = ['<b>', $godziny, ':', ($minuty < 10) ? '0'.concat($minuty) : $minuty, ':', ($sekundy < 10) ? '0'.concat($sekundy) : $sekundy, '</b>'].join('');
            document.getElementById("zegarLayer").innerHTML = $czas;
            $timerID = setTimeout(wyswietlCzas, 1000);
        }
        window.onload = wyswietlCzas;

    </script>

    <script type="text/javascript">
        var sur1 = 100;
        var sur1h = 500;

        function sur() {
            sur1_sec = sur1h / 3600;
            sur1 = sur1 + sur1_sec;
            document.getElementById("sur1").innerHTML = sur1;
        }
        setInterval("sur()", 1000);

    </script>



</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>
            
            <div class="nav">
            <ol>
                <li><a class="menu" href="gra.php">Okolice</a></li>
                <li><a class="menu" href="osada.php">Osada</a></li>
                <li><a class="menu active" href="mapa.php">Mapa</a></li>
                <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                <li><a class="menu" href="raporty.php">Raporty</a></li>
                <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

            </ol>
        </div>
            
            <article>
                <div class="mapa-area">

                    
                        <div class="wyloguj">
                            <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>
                    
                        MAPA
                        
                        
                                       <div class="dane">
                        <?php
	echo "<p><b>E-mail</b>: ".$_SESSION['email'];
	echo "<br /><b>Data wygaśnięcia premium</b>: ".$_SESSION['dnipremium']."</p>";
	                 
	$dataczas = new DateTime();
	
	echo "Data i czas serwera: ".$dataczas->format('Y-m-d H:i:s')."<br>";
	
	$koniec = DateTime::createFromFormat('Y-m-d H:i:s', $_SESSION['dnipremium']);
	
	$roznica = $dataczas->diff($koniec);
	
	if($dataczas<$koniec)
	echo "Pozostało premium: ".$roznica->format('%y lat, %m mies, %d dni, %h godz, %i min, %s sek');
	else
	echo "Premium nieaktywne od: ".$roznica->format('%y lat, %m mies, %d dni, %h godz, %i min, %s sek');	
	
?>
                            
                            <div id="zegarLayer">
                            </div>

                            <div id="sur1"></div>
                            
                        </div>
                    



            </article>
        </section>
    </div>
    <footer>Arkadiusz Wajs | Osadnicy | 2020
    </footer>
    </div>
</body>

</html>
<?php exit;?>
