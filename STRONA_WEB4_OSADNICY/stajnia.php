<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>




<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu active" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>

                <div class="surowce-area">

                    <div class="wyloguj-surowce">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?></div>


                    <div class="zasoby-surowce">
                        <?php
	echo "<p><b>Drewno</b>: ".$_SESSION['drewno'];
    echo " | <b>Glina</b>: ".$_SESSION['glina'];
    echo " | <b>Kamień</b>: ".$_SESSION['kamien'];
    echo " | <b>Zboże</b>: ".$_SESSION['zboze']."</p>";
                                               
	           ?>


                    </div>

                    <div class="surowce">
                        <h3>Stajnia</h3>
                        </br>
                        <p>Wymagania: główny budynek poziom 3, koszary poziom 5</p>
                        </br>
                        <p>W stajni można trenować kawalerię. Im wyższy poziom tym szybciej trenowane są jednostki.</p>
                        </br>
                        <p>Ciekawostki:</br>
                            * Czas szkolenia jednostek jest zależny od poziomu Stajni w momencie załączenia szkolenia. Czyli np. jeśli załączymy budowę 500 Paladynów na 10 poziomie Stajni, a następnie po wyszkoleniu połowy ulepszymy Stajnię na 20 poziom, to mimo wyższego poziomu budynku, reszta Paladynów będzie się szkoliła w takim samym czasie, dopiero jednostki które się załączy do szkolenia już po ulepszeniu Stajni, będą się szybciej szkoliły.</p>
                        <img src="icon/stajnia.png">

                    </div>
                </div>
            </article>

        </section>



        <footer>Arkadiusz Wajs | Osadnicy | 2020
        </footer>
    </div>

</body>

</html>
<?php exit;?>
