<?php

	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
	
?>

<?php
date_default_timezone_set('Europe/Warsaw');
function militime(){
    $time = explode(' ',microtime(),2);
    return floor(($time[1]+$time[0])*1000);
};?>


<?php
require_once "connect.php";
?>


<!DOCTYPE HTML>
<html lang="pl">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Osadnicy - gra przeglądarkowa</title>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
    <link rel="icon" href="ikona.ico">
    <link rel="shortcut icon" href="ikona.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="main.css" />



    <script>
        var $timerID = null,
            $dif = (new Date(<?php echo militime();?>)).getTime() - (new Date()).getTime();

        function wyswietlCzas() {
            var $data = new Date((new Date()).getTime() + $dif),
                $godziny = $data.getHours(),
                $minuty = $data.getMinutes(),
                $sekundy = $data.getSeconds(),
                $czas = ['<b>', $godziny, ':', ($minuty < 10) ? '0'.concat($minuty) : $minuty, ':', ($sekundy < 10) ? '0'.concat($sekundy) : $sekundy, '</b>'].join('');
            document.getElementById("zegarLayer").innerHTML = $czas;
            $timerID = setTimeout(wyswietlCzas, 1000);
        }
        window.onload = wyswietlCzas;

    </script>





</head>

<body onload="wyswietlCzas();">
    <div id="wrapper">
        <header>

            <span style="color:  #c34f4f">Osadnicy</span>
        </header>
        <section>

            <div class="nav">
                <ol>
                    <li><a class="menu" href="gra.php">Okolice</a></li>
                    <li><a class="menu" href="osada.php">Osada</a></li>
                    <li><a class="menu" href="mapa.php">Mapa</a></li>
                    <li><a class="menu active" href="statystyki.php">Statystyki</a></li>
                    <li><a class="menu" href="raporty.php">Raporty</a></li>
                    <li><a class="menu" href="wiadomosci.php">Wiadomości</a></li>

                </ol>
            </div>

            <article>
                <div class="statystyki-area">


                    <div class="wyloguj-statystyki">
                        <?php
	echo "Witaj ".$_SESSION['user'].'! [ <a href="logout.php">Wyloguj się!</a> ]';
                ?>

                    </div>
                    </br>
                    
                    <div class="zegar" id="zegarLayer"></div>

                    <div class="statystyki">
                        <h3>Statystyki</h3></br>
                
<?php            
            // Create connection
$conn = new mysqli($host, $db_user, $db_password, $db_name);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

function t1($val, $min, $max){
    return ($val >= $min && $val <= $max);
}                
                
$query = $conn->query('SELECT id FROM uzytkownicy ORDER BY id'); 
$row = mysqli_num_rows($query);              
                
$page = isSet( $_GET['page'] ) ? intval( $_GET['page'] -1 ) : 1;
$limit = 10;
$from = $page * $limit;
$allPage = ceil($row / $limit);                
$sql = "SELECT id, user FROM uzytkownicy ORDER BY id ASC LIMIT $from, $limit ";
                
//echo 'PAGE: ' . $page . '</br>';
//echo 'ALL PAGE: ' . $allPage . '</br>';               
//echo 'COUNT: ' . $row . '</br>';                
//echo 'LIMIT: ' . $limit . '</br>';
//echo 'FROM: ' . $from . '</br>';
//echo 'SQL: ' . $sql . '</br>';                
                
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table><tr><th>ID</th><th>Gracz</th></tr>";
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<tr><td>".$row["id"]."</td><td>".$row["user"]."</td></tr>";
  }
  echo "</table>";
    
    if($page > 5){
        echo '<a href="statystyki.php?page=1"><pierwsza strona </a> | ';
    }
    
    for( $i = 1; $i <= $allPage; $i++){
        $bold = ( $i == ($page +1 ) ) ? 'style="font-size: 20px"' : '';
        
        if(t1($i, ($page -3),($page +5))){
            
        echo '<a ' .$bold . ' href="statystyki.php?page=' . $i . '">'.$i.'</a> | ';
        }
    }
    
    if($page < ($allPage -1)){
        echo '<a href="statystyki.php?page=' .$allPage.'"> ostatnia strona> </a> ';
    }
    
} else {
  echo "0 results";
}
$conn->close();
?>
                    </div>

                </div>

            </article>
        </section>
    </div>
    <footer>Arkadiusz Wajs | Osadnicy | 2020
    </footer>
    </div>
</body>

</html>
<?php exit;?>
